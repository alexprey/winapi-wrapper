///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)
///	AWF.h
///		Main framework header

#ifndef __AWF_H
#define __AWF_H

#include "Core\Application.h"

using namespace std;

using namespace AWF::Core;

namespace AWF
{
	Application* pApplication;
}

#endif