///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __BASIC_CONTROLS
#define __BASIC_CONTROLS

#include "Misc\Container.h"

#include "Controls\UIForm.h"
#include "Controls\UIButton.h"
#include "Controls\UILabel.h"
#include "Controls\UITextEdit.h"
#include "Controls\UIPanel.h"

using namespace AWF::Misc;
using namespace AWF::Controls;

#endif