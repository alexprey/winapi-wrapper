#include "UIButton.h"

namespace AWF
{
	namespace Controls
	{

		HRESULT UIButton::Initialize(int positionX, int positionY, int width, int height, AbstractWindow* pWindow)
		{
			return TextedAbstractWindow::Initialize(
				positionX, positionY,					// positioning
				width, height,							// container size
				GetDefaultStyle(),							// styles
				GetDefaultStyleEx(),									// ex styles
				pWindow,								// parent
				L"Button"								// default window class
				);
		}

	}
}