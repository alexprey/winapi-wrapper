///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)
///	UIButton.h
///		Simple button

#ifndef __UI_BUTTON_H
#define __UI_BUTTON_H

#include <AWF\Core\SimpleTextedControl.h>

using namespace AWF::Core;

namespace AWF
{
	namespace Controls
	{

		class UIButton : public SimpleTextedControl
		{
		public:
			UIButton(HINSTANCE hInstance) : SimpleTextedControl(hInstance) {}
			virtual ~UIButton() {}

			virtual HRESULT Initialize(int positionX, int positionY, int width, int height, AbstractWindow* pWindow = NULL);
		};

	}
}

#endif 