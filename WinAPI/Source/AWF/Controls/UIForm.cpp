#include "UIForm.h"

namespace AWF
{
	namespace Controls
	{
		UIForm::UIForm(HINSTANCE hInstance) : TextedAbstractWindow(hInstance)
		{

		}

		UIForm::~UIForm()
		{

		}

		void UIForm::GetRect(Rect* pRect)
		{
			if (pRect)
			{
				RECT rect;
				GetClientRect(GetHandle(), &rect);
				pRect->left = rect.left;
				pRect->right = rect.right;
				pRect->top = rect.top;
				pRect->bottom = rect.bottom;
			}
		}

		int UIForm::GetWidth()
		{
			RECT rect;
			GetClientRect(GetHandle(), &rect);
			return rect.right - rect.left;
		}

		int UIForm::GetHeight()
		{
			RECT rect;
			GetClientRect(GetHandle(), &rect);
			return rect.bottom - rect.top;
		}

		DWORD UIForm::GetDefaultStyle()
		{
			return WS_OVERLAPPEDWINDOW | WS_VISIBLE;
		}

		HRESULT UIForm::Initialize(int x, int y, int width, int height)
		{
			return TextedAbstractWindow::Initialize(x, y, width, height, NULL, NULL, NULL, NULL);
		}
	}
}