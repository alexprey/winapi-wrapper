///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __UI_FORM_H
#define __UI_FORM_H

#include <AWF\Core\TextedAbstractWindow.h>

using namespace AWF::Core;

namespace AWF
{
	namespace Controls
	{

		class UIForm : public TextedAbstractWindow
		{
		public:
			UIForm(HINSTANCE hInstance);
			virtual ~UIForm();

			virtual int GetWidth();
			virtual int GetHeight();

			virtual void GetRect(Rect* pRect);

			virtual DWORD GetDefaultStyle();
			virtual HRESULT Initialize(int x, int y, int width, int height);
		};

	}
}

#endif