#include "UILabel.h"

namespace AWF
{
	namespace Controls
	{
		HRESULT UILabel::Initialize(int positionX, int positionY, int width, int height, AbstractWindow* pWindow)
		{
			return TextedAbstractWindow::Initialize(
				positionX, positionY,					// positioning
				width, height,							// container size
				WS_CHILD | WS_VISIBLE | SS_LEFT,		// styles
				NULL,									// ex styles
				pWindow,								// parent
				L"Static"								// default window class
				);
		}
	}
}