///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)
///	UILabel.h
///		Simple text control

#ifndef __UI_LABEL_H
#define __UI_LABEL_H

#include <AWF\Core\SimpleTextedControl.h>

using namespace AWF::Core;

namespace AWF
{
	namespace Controls
	{

		class UILabel : public SimpleTextedControl
		{
		public:
			UILabel(HINSTANCE hInstance) : SimpleTextedControl(hInstance) {}
			virtual ~UILabel() {}

			HRESULT Initialize(int positionX, int positionY, int width, int height, AbstractWindow* pWindow = NULL);
		};

	}
}

#endif 