#include "UIListView.h"
#include <Windows.h>
#include <Commctrl.h>
#include <strsafe.h>

namespace AWF
{
	namespace Controls
	{
		LRESULT UIListView::NotifyHandler(AbstractWindow* pWindow, const MSG& msg, DWORD& hfFlags)
		{
			NMLVDISPINFO* plvdi;
			switch (((LPNMHDR)msg.lParam)->code)
			{
				case LVN_GETDISPINFO:
					{
						hfFlags |= HANDLER_FLAG_HANDLED;
						hfFlags &= ~HANDLER_FLAG_USE_DEFAULT;
						plvdi = (NMLVDISPINFO*)msg.lParam;
						UIListView* pListView = (UIListView*)AbstractWindow::GetWindow(((LPNMHDR)msg.lParam)->hwndFrom);
						if (pListView->pBindedData)
						{
							UIListViewColumn* pColumn = pListView->columnList[plvdi->item.iSubItem];
							if (pColumn)
							{
								IDataProvider* pData = pListView->pBindedData->at(plvdi->item.iItem);
								wstring propertyName = pColumn->GetBindedProperty();
								if (pData && pData->IsHasProperty(propertyName))
								{
									wstring text(pData->GetProperty(propertyName));
									StringCchCopy(plvdi->item.pszText, plvdi->item.cchTextMax, text.c_str());
								}
							}
						}
					} break;
			}

			return 0;
		}

		LRESULT UIListView::RepaintHandler(AbstractWindow* pWindow, const MSG& msg, DWORD& hfFlags)
		{
			UIListView* pListView = (UIListView*)pWindow;
			if (pListView)
			{
				LVITEM item = {0};
				RECT rect;
				item.iItem = 0;
				item.iSubItem = 0;

				//ListView_IsItemVisible(pListView->GetHandle(), idx);
				ListView_GetItem(pListView->GetHandle(), &item);
				ListView_GetItemRect(pListView->GetHandle(), item.iItem, &rect, LVIR_BOUNDS);
			}

			return 0;
		}

		UIListView::UIListView(HINSTANCE hInstance) : SimpleControl(hInstance)
		{

		}

		UIListView::~UIListView()
		{

		}

		DWORD UIListView::GetDefaultStyleEx()
		{
			return SimpleControl::GetDefaultStyleEx();
		}

		DWORD UIListView::GetDefaultStyle()
		{
			return SimpleControl::GetDefaultStyle() | LVS_REPORT | WS_BORDER;
		}

		void UIListView::OnCreate()
		{
			if (this->GetParent())
				this->GetParent()->RegisterMessageHandler(WM_NOTIFY, &UIListView::NotifyHandler);

			ListView_SetExtendedListViewStyle(GetHandle(), LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
		}

		HRESULT UIListView::Initialize(int positionX, int positionY, int width, int height, AbstractWindow* pWindow)
		{
			INITCOMMONCONTROLSEX icex;
			icex.dwICC = ICC_LISTVIEW_CLASSES;
			InitCommonControlsEx(&icex);

			return SimpleControl::Initialize(
				positionX, positionY,					// positioning
				width, height,							// container size
				NULL,									// styles
				NULL,									// ex styles
				pWindow,								// parent
				WC_LISTVIEW								// default window class
				);
		}

		UIListViewColumn* UIListView::GetHeader(int idx)
		{
			if (idx >= 0 && idx < columnList.size())
				return columnList.at(idx);

			return NULL;
		}

		int UIListView::GetColumnCount()
		{
			return columnList.size();
		}

		void UIListView::BindData(vector<IDataProvider*>* pData)
		{
			this->pBindedData = pData;
		}

		void UIListView::InsertColumn(UIListViewColumnDef& columnDef, int idx)
		{
			columnList.insert(columnList.end(), new UIListViewColumn(columnDef, this));
		}

		int UIListView::GetFirstSelectItem()
		{
			return ListView_GetNextItem(GetHandle(), -1, LVNI_SELECTED);
		}

		void UIListView::UpdateBindedData()
		{
			ListView_DeleteAllItems(GetHandle());
			if (pBindedData)
			{
				int itemCount = 0;
				for (auto it = pBindedData->begin(); it != pBindedData->end(); it++)
				{
					IDataProvider* pDataProvider = *it;

					LVITEM lvi;
					lvi.pszText = L"";
					lvi.mask = LVIF_TEXT | LVIF_STATE;
					lvi.stateMask = 0;
					lvi.state = 0;
					lvi.iItem = itemCount++;
					lvi.iSubItem = 0;

					ListView_InsertItem(GetHandle(), &lvi);
					ListView_SetItemText(GetHandle(), itemCount - 1, 0, LPSTR_TEXTCALLBACK);
				}
			}
		}

		void UIListView::UpdateHeader()
		{
			int columnCount = Header_GetItemCount(ListView_GetHeader(GetHandle()));
			while (columnCount-- > 0)
				ListView_DeleteColumn(GetHandle(), 0);

			LVCOLUMN lvColumn;
			UIListViewColumn* pColumn;
			columnCount = 0;
			for (auto it = columnList.begin(); it != columnList.end(); it++)
			{
				pColumn = *it;
				lvColumn.iSubItem = columnCount;
				lvColumn.cx = pColumn->GetWidth();
				wstring* pTitle = new wstring(pColumn->GetTitle());
				lvColumn.pszText = (LPWSTR)pTitle->c_str();

				ListView_InsertColumn(GetHandle(), columnCount, &lvColumn);
				ListView_SetColumnWidth(GetHandle(), columnCount, pColumn->GetWidth());

				columnCount++;
			}
		}

		void UIListView::Update()
		{
			UpdateHeader();
			UpdateBindedData();
		}

	}
}