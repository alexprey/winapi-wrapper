///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __UI_LIST_VIEW_H
#define __UI_LIST_VIEW_H

#include <vector>
#include <AWF\Core\SimpleControl.h>
#include <AWF\Misc\IDataProvider.h>
#include "UIListView\UIListView-Common.h"
#include "UIListView\UIListView-Column.h"
#include "UIListView\UIListView-Item.h"

using namespace std;
using namespace AWF::Core;
using namespace AWF::Misc;

namespace AWF
{
	namespace Controls
	{
		class UIListView : public SimpleControl
		{
		private:
			static LRESULT NotifyHandler(AbstractWindow* pWindow, const MSG& msg, DWORD& hfFlags);
			static LRESULT RepaintHandler(AbstractWindow* pWindow, const MSG& msg, DWORD& hfFlags);
		private:
			vector<IDataProvider*>* pBindedData;
			vector<UIListViewColumn*> columnList;
		protected:
			virtual void OnCreate();
		public:
			UIListView(HINSTANCE hInstance);
			virtual ~UIListView();

			virtual DWORD GetDefaultStyleEx();
			virtual DWORD GetDefaultStyle();

			virtual HRESULT Initialize(int positionX, int positionY, int width, int height, AbstractWindow* pWindow = NULL);

			virtual UIListViewColumn* GetHeader(int idx);
			virtual int GetColumnCount();
			virtual void InsertColumn(UIListViewColumnDef& columnDef, int idx = -1);

			int GetFirstSelectItem();

			virtual void BindData(vector<IDataProvider*>* pData);
			
			virtual void UpdateBindedData();
			virtual void UpdateHeader();
			virtual void Update();
		};
	}
}

#endif