///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __UI_LIST_VIEW_COLUMN_H
#define __UI_LIST_VIEW_COLUMN_H

#include <AWF\Core\AbstractWindow.h>
#include "UIListView-Common.h"

using namespace AWF::Core;

namespace AWF
{
	namespace Controls
	{
		class UIListViewColumn
		{
		private:
			UIListViewColumnDef _columnDef;
		public:
			UIListViewColumn(UIListViewColumnDef& columnDef, AbstractWindow* pParent) : _columnDef(columnDef)
			{
			}

			int GetWidth() { return _columnDef.Width; }

			wstring GetTitle() { return _columnDef.Title; }

			wstring GetBindedProperty() { return _columnDef.BindedProperty; }
		};
	}
}

#endif