///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __UI_LIST_VIEW_COMMON_H
#define __UI_LIST_VIEW_COMMON_H

#include <string>

using namespace std;

namespace AWF
{
	namespace Controls
	{	
		struct UIListViewColumnDef
		{
			wstring Title;
			wstring BindedProperty;
			int Width;

			UIListViewColumnDef(UIListViewColumnDef& object)
			{
				Title = object.Title;
				Width = object.Width;
				BindedProperty = object.BindedProperty;
			}

			UIListViewColumnDef(wstring Title, int Width = 100, wstring BindedProperty = L"")
			{
				this->Title = Title;
				this->Width = Width;
				this->BindedProperty = BindedProperty;
			}
		};
	}
}

#endif