#include "UIPanel.h"

namespace AWF
{
	namespace Controls
	{
		LRESULT UIPanel::Initialize(int x, int y, int width, int height, DWORD dwStyle, AbstractWindow* pParent)
		{
			return AbstractWindow::Initialize(x, y, width, height, dwStyle, NULL, pParent, L"Static");
		}
	}
}