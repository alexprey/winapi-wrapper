///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __UI_PANEL_H
#define __UI_PANEL_H

#include <AWF\Core\AbstractWindow.h>

using namespace AWF::Core;

namespace AWF
{
	namespace Controls
	{
		class UIPanel : public AbstractWindow
		{
		public:
			UIPanel(HINSTANCE hInstance) : AbstractWindow(hInstance) {}
			virtual ~UIPanel() {}

			virtual LRESULT Initialize(int x, int y, int width, int height, DWORD dwStyle, AbstractWindow* pParent);
		};
	}
}

#endif