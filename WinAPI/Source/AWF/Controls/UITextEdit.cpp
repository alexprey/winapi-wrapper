#include "UITextEdit.h"

namespace AWF
{
	namespace Controls
	{
		DWORD UITextEdit::GetDefaultStyle()
		{
			return TextedAbstractWindow::GetDefaultStyle() | WS_BORDER;
		}

		HRESULT UITextEdit::Initialize(int positionX, int positionY, int width, int height, AbstractWindow* pWindow)
		{
			return AbstractWindow::Initialize(
				positionX, positionY,					// positioning
				width, height,							// container size
				NULL,									// styles
				NULL,									// ex styles
				pWindow,								// parent
				L"EDIT"									// default window class
				);
		}
	}
}