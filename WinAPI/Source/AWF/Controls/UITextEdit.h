///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __TEXT_EDIT_H
#define __TEXT_EDIT_H

#include <AWF\Core\SimpleTextedControl.h>

using namespace AWF;
using namespace AWF::Core;

namespace AWF
{
	namespace Controls
	{
		class UITextEdit : public SimpleTextedControl
		{
		private:
			bool isReadOnly;
		public:
			UITextEdit(HINSTANCE hInstance) : SimpleTextedControl(hInstance) {}
			virtual ~UITextEdit() {}

			virtual DWORD GetDefaultStyle();

			virtual HRESULT Initialize(int positionX, int positionY, int width, int height, AbstractWindow* pWindow = NULL);
		};
	}
}

#endif