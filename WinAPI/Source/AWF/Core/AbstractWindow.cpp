#include "AbstractWindow.h"
#include "Application.h"

#include <CommCtrl.h>

namespace AWF
{
	namespace Core
	{
		unordered_map<HWND, AbstractWindow*> windowsMap;

		void AbstractWindow::RegisterWindow(HWND hWnd, AbstractWindow* pWindow) {
			windowsMap[hWnd] = pWindow;
		}

		void AbstractWindow::UnRegisterWindow(HWND hWnd) {
			windowsMap.erase(hWnd);
		}

		AbstractWindow* AbstractWindow::GetWindow(HWND hWnd)
		{
			return windowsMap[hWnd];
		}

		LRESULT AbstractWindow::ProcedHandlers(UINT msg, WPARAM wParam, LPARAM lParam, DWORD& hfFlags)
		{
			auto pHandlerVector = winMsgHandlerTable[msg];
			if (pHandlerVector)
			{
				for(auto it = pHandlerVector->begin(); it != pHandlerVector->end(); it++)
				{
					auto pHandler = *it;
					if (pHandler) {
						MSG message;
						DWORD _hfFlags = hfFlags;
						message.hwnd = hWnd;
						message.message = msg;
						message.lParam = lParam;
						message.wParam = wParam;
						LRESULT code = pHandler(this, message, _hfFlags);
						if (_hfFlags & HANDLER_FLAG_HANDLED)
						{
							hfFlags = _hfFlags;
							return code;
						}
					}
				}
			}

			return 0;
		}

		LRESULT CALLBACK AbstractWindow::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
			//ProcedWindowMessage(hWnd, msg, wParam, lParam);

			if (msg == WM_DESTROY)
			{
				PostQuitMessage(0);
				return 0;
			}

			AbstractWindow* pWindow = AbstractWindow::GetWindow(hWnd);
			if (pWindow)
			{
				if (msg == WM_SIZE)
				{
					pWindow->OnResize(LOWORD(lParam), HIWORD(lParam));
				}

				DWORD hfFlags = HANDLER_FLAG_USE_DEFAULT;
				LRESULT handledCode = pWindow->ProcedHandlers(msg, wParam, lParam, hfFlags);

				if ((hfFlags & HANDLER_FLAG_USE_DEFAULT) || !(hfFlags & HANDLER_FLAG_HANDLED))
				{
					LRESULT defCode = 0;
					bool isHasDefaultProc = pWindow->pStdWindowProc && (pWindow->pStdWindowProc != &WndProc);
					if (isHasDefaultProc)
						defCode = pWindow->pStdWindowProc(hWnd, msg, wParam, lParam);

					if ((hfFlags & HANDLER_FLAG_HANDLED) && (hfFlags & HANDLER_FLAG_RETURN_HANDLED))
						return handledCode;

					if (isHasDefaultProc)
						return defCode;
				}
			}

			return DefWindowProc(hWnd, msg, wParam, lParam);;
		}

		bool AbstractWindow::RegisterMessageHandler(UINT message, WinMessageHandlerFunction pHandler) {
			vector<WinMessageHandlerFunction>* pHandlerVector = winMsgHandlerTable[message];
			if (pHandlerVector)
			{
				for (auto it = pHandlerVector->begin(); it != pHandlerVector->end(); it++)
					if (*it == pHandler)
						return false;
			} else {
				pHandlerVector = new vector<WinMessageHandlerFunction>();
				winMsgHandlerTable[message] = pHandlerVector;
			}

			pHandlerVector->insert(pHandlerVector->end(), pHandler);
			return true;
		}

		bool AbstractWindow::UnRegisterMessageHandler(UINT message, WinMessageHandlerFunction pHandler) {
			vector<WinMessageHandlerFunction>* pHandlerVector = winMsgHandlerTable[message];
			if (pHandlerVector)
			{
				for (auto it = pHandlerVector->begin(); it != pHandlerVector->end(); it++)
					if (*it == pHandler) {
						pHandlerVector->erase(it);
						if (pHandlerVector->size() == 0)
							UnRegisterMessageHandler(message);
						return true;
					}
			}
			return false;
		}

		void AbstractWindow::UnRegisterMessageHandler(UINT message) {
			vector<WinMessageHandlerFunction>* pHandlerVector = winMsgHandlerTable[message];
			if (pHandlerVector)
				delete pHandlerVector;
			winMsgHandlerTable.erase(message);
		}

		TCHAR lpWindowClass[] = _T("AWFstdFormClass");
		TCHAR lpWindowTitle[] = _T("AWF std form");

		bool AbstractWindow::_IsInitWindowClass = false;

		void AbstractWindow::_InitWindowClass() {
			if (!_IsInitWindowClass) {
				_IsInitWindowClass = true;

				WNDCLASSEX wcex = {};
				wcex.cbSize = sizeof(WNDCLASSEX);

				wcex.lpfnWndProc = WndProc;
				wcex.hInstance = 0;
				wcex.hIcon = LoadIcon(0, IDI_APPLICATION);
				wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
				wcex.hbrBackground = GetSysColorBrush(COLOR_BTNFACE);
				wcex.lpszClassName = lpWindowClass;
				wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

				if (!RegisterClassEx(&wcex)) {
					Application::ErrorExit(_T("RegisterClassEx"));
				}
			}
		}

		void AbstractWindow::OnResize(int width, int height)
		{
			rect.right = rect.left + width;
			rect.bottom = rect.top + height;

			if (pLayoutProvider) {
				pLayoutProvider->SetWidth(width);
				pLayoutProvider->SetHeight(height);
			}

			UpdateLayout();
		}

		DWORD AbstractWindow::GetStyle()
		{
			return GetWindowLong(GetHandle(), GWL_STYLE);
		}

		void AbstractWindow::SetStyle(DWORD newStyle)
		{
			SetWindowLong(GetHandle(), GWL_STYLE, newStyle);
			Invalidate();
		}

		DWORD AbstractWindow::GetDefaultStyle()
		{
			DWORD dwStyle = NULL;
			if (pParentWindow)
				dwStyle |= WS_CHILD;

			dwStyle |= WS_VISIBLE;

			return dwStyle;
		}

		DWORD AbstractWindow::GetDefaultStyleEx()
		{
			DWORD dwExStyle = NULL;

			return dwExStyle;
		}

		HRESULT AbstractWindow::Initialize(int positionX, int positionY, int width, int height, DWORD dwWindowStyle, DWORD dwExWindowStyle, AbstractWindow* pParent, LPWSTR szClassName) {
			if (!szClassName) {
				_InitWindowClass();
				szClassName = lpWindowClass;
			}

			pParentWindow = pParent;
			HWND hWndParent = NULL;
			if (pParent && pParent->CanProvideParent()) {
				hWndParent = pParent->GetHandle();
				pParent->AddChild(this);
			}

			container.SetLeftTop(positionX, positionY, width, height);
			rect.left = positionX;
			rect.top = positionY;
			rect.right = rect.left + width;
			rect.bottom = rect.top + height;

			if (!(hWnd = CreateWindowEx(
					GetDefaultStyleEx() | dwExWindowStyle, 
					szClassName, 
					L"", 
					GetDefaultStyle() | dwWindowStyle, 
					positionX, positionY, width, height, 
					hWndParent, 
					NULL, 
					hInstance, 
					NULL))) {
				Application::ErrorExit(_T("CreateWindowEx"));
			}
			AbstractWindow::RegisterWindow(hWnd, this);
			pStdWindowProc = (WNDPROC)GetWindowLong(hWnd, GWL_WNDPROC);
			if (pStdWindowProc != &WndProc)
				SetWindowLong(hWnd, GWL_WNDPROC, (LONG)&WndProc);
			OnCreate();

			return S_OK;
		};

		int AbstractWindow::GetChildCount()
		{
			if (pChildList)
				return pChildList->size();
			return 0;
		}

		AbstractWindow* AbstractWindow::GetChild(int idx)
		{
			if (pChildList)
				return pChildList->at(idx);
			return nullptr;
		}

		AbstractWindow* AbstractWindow::GetParent()
		{
			return pParentWindow;
		}

		void AbstractWindow::SetParent(AbstractWindow* pNewParent)
		{
			if (pNewParent->CanProvideParent())
			{
				if (this->pParentWindow)
					this->pParentWindow->DeleteChild(this);

				this->pParentWindow = pNewParent;

				if (this->pParentWindow)
					this->pParentWindow->AddChild(this);
			}
		}

		void AbstractWindow::AddChild(AbstractWindow* pWindow)
		{
			if (pWindow && this->CanProvideParent())
			{
				bool isNeedSetNewParent = false;
				bool isNeedAddInChildList = true;

				if (pChildList) {
					for (auto it = pChildList->begin(); it != pChildList->end(); it++)
						if (*it == pWindow)
						{
							isNeedSetNewParent = (*it)->pParentWindow != pWindow;
							isNeedAddInChildList = false;
							break;
						}
				} else {
					pChildList = new vector<AbstractWindow*>();
					if (pLayoutProvider)
						pLayoutProvider->SetChildVector(pChildList);
				}

				if (isNeedSetNewParent || isNeedAddInChildList)
				{
					if (pWindow->pParentWindow != NULL && pWindow->pParentWindow != this)
						pWindow->pParentWindow->DeleteChild(pWindow);
					pWindow->pParentWindow = this;

					if (isNeedAddInChildList)
						pChildList->insert(pChildList->end(), pWindow);
					pWindow->UpdateRect();
				}
			}
		}

		void AbstractWindow::DeleteChild(AbstractWindow* pWindow)
		{
			if (pChildList) {
				if (pWindow && this->CanProvideParent())
				{
					for (auto it = pChildList->begin(); it != pChildList->end(); it++) {
						if (*it == pWindow)
						{
							pChildList->erase(it);
							if (pWindow->pParentWindow == this)
								pWindow->pParentWindow = NULL;
							return;
						}
					}
					if (pChildList->empty()) {
						delete pChildList;
						pChildList = nullptr;
						if (pLayoutProvider)
							pLayoutProvider->SetChildVector(pChildList);
					}
				}
			}
		}

		void AbstractWindow::GetRect(Rect* pRect)
		{
			if (pRect)
			{
				pRect->left = GetPositionX();
				pRect->top = GetPositionY();
				pRect->right = pRect->left + GetWidth();
				pRect->bottom = pRect->top + GetHeight();					
			}
		}

		void AbstractWindow::ApplyRect(Rect* pRect)
		{
			if (pRect)
			{
				UINT flags = NULL;

				if ((rect.left == pRect->left) && (rect.top == pRect->top))
					flags |= SWP_NOMOVE;

				if ((rect.GetWidth() == pRect->GetWidth()) && rect.GetHeight() == pRect->GetHeight())
					flags |= SWP_NOSIZE;

				rect.left = pRect->left;
				rect.right = pRect->right;
				rect.top = pRect->top;
				rect.bottom = pRect->bottom;

				SetWindowPos(this->hWnd, NULL, pRect->left, pRect->top, pRect->GetWidth(), pRect->GetHeight(), flags | SWP_NOZORDER);
			}
		}

		void AbstractWindow::SetPosition(int newPositionX, int newPositionY)
		{
			container.marginLeft = newPositionX;
			container.marginTop = newPositionY;

			UpdateRect();
		}

		int AbstractWindow::GetPositionX()
		{
			return this->rect.left;
		}

		int AbstractWindow::GetPositionY()
		{
			return this->rect.top;
		}

		void AbstractWindow::SetSize(int newWidth, int newHeight)
		{
			this->container.width = newWidth;
			this->container.height = newHeight;

			UpdateRect();
		}

		int AbstractWindow::GetWidth()
		{
			return rect.GetWidth();
		}

		int AbstractWindow::GetContainerWidth()
		{
			return container.width;
		}

		int AbstractWindow::GetHeight()
		{
			return rect.GetHeight();
		}

		int AbstractWindow::GetContainerHeight()
		{
			return container.height;
		}

		void AbstractWindow::SetMargin(int newMarginRight, int newMarginBottom)
		{
			if ((container.marginRight != newMarginRight) || (container.marginBottom != newMarginBottom))
			{
				container.marginRight = newMarginRight;
				container.marginBottom = newMarginBottom;

				UpdateRect();
			}
		}

		int AbstractWindow::GetMarginRight()
		{
			return container.marginRight;
		}

		int AbstractWindow::GetMarginBottom()
		{
			return container.marginBottom;
		}

		void AbstractWindow::SetContainerFlags(char flags)
		{
			if (container.flags != flags)
			{
				container.flags = flags;

				UpdateRect();
			}
		}

		char AbstractWindow::GetContainerFlags()
		{
			return container.flags;
		}

		void AbstractWindow::Show(int show) 
		{
			ShowWindow(hWnd, show);
		}

		void AbstractWindow::SetLayoutProvider(ILayoutProvider* pLayoutProvider)
		{
			if (this->pLayoutProvider != pLayoutProvider)
			{
				this->pLayoutProvider = pLayoutProvider;
				if (pLayoutProvider) {
					pLayoutProvider->SetWidth(GetWidth());
					pLayoutProvider->SetHeight(GetHeight());
					pLayoutProvider->SetChildVector(pChildList);
				}

				UpdateLayout();
			}
		}

		ILayoutProvider* AbstractWindow::GetLayoutProvider()
		{
			return pLayoutProvider;
		}

		void AbstractWindow::UpdateLayout()
		{
			if (pLayoutProvider) {
				if (pLayoutProvider->IsFullRecompute()) {
					pLayoutProvider->RecomputeContainer();
				} else {
					if (pChildList) {
						Rect* pRect = new Rect();
						for(auto it = pChildList->begin(); it != pChildList->end(); it++) {
							pLayoutProvider->RecomputeContainer(&(*it)->container, pRect);
							(*it)->ApplyRect(pRect);
						}
						delete pRect;
					}
				}
			} else {
				if (pChildList) {
					for(auto it = pChildList->begin(); it != pChildList->end(); it++)
						(*it)->UpdateRect();
				}
			}
		}

		bool AbstractWindow::UpdateLayout(const Container* pContainer, Rect* pWindowRect)
		{
			bool isFullRecompute = pLayoutProvider->IsFullRecompute();
			if (isFullRecompute) {
				pLayoutProvider->RecomputeContainer();
			} else {
				pLayoutProvider->RecomputeContainer(pContainer, pWindowRect);
			}

			return !isFullRecompute;
		}

		void AbstractWindow::ApplyMargedRect(Rect* pRect)
		{
			if (pRect)
			{
				Rect mRect;
				mRect.left = pRect->left + container.marginLeft;
				mRect.top = pRect->top + container.marginTop;
				mRect.right = mRect.left + container.width;
				mRect.bottom = mRect.top + container.height;

				if (container.flags & CF_FIX_RIGHT)
					mRect.right = pRect->right - container.marginRight;
				if (container.flags & CF_FIX_BOTTOM)
					mRect.bottom = pRect->bottom - container.marginBottom;

				if (!(container.flags & CF_FIX_LEFT))
					mRect.left = mRect.right - container.width;
				if (!(container.flags & CF_FIX_TOP))
					mRect.top = mRect.bottom - container.height;

				ApplyRect(&mRect);
			}
		}

		void AbstractWindow::UpdateRect() 
		{
			Rect* pRect = new Rect();
			bool needApplyRect = true;

			if (pParentWindow && pParentWindow->pLayoutProvider) {
				needApplyRect = pParentWindow->UpdateLayout(&container, pRect);
			} else {
				pRect->left = container.marginLeft;
				pRect->top = container.marginTop;
				pRect->right = pRect->left + container.width;
				pRect->bottom = pRect->top + container.height;

				if (pParentWindow)
				{
					if (container.flags & CF_FIX_RIGHT)
						pRect->right = pParentWindow->GetWidth() - container.marginRight;
					if (container.flags & CF_FIX_BOTTOM)
						pRect->bottom = pParentWindow->GetHeight() - container.marginBottom;

					if (!(container.flags & CF_FIX_LEFT))
						pRect->left = pRect->right - container.width;
					if (!(container.flags & CF_FIX_TOP))
						pRect->top = pRect->bottom - container.height;
				}		
			}

			if (needApplyRect)
				ApplyRect(pRect);

			delete pRect;
		}

		bool AbstractWindow::IsVisible() {
			return GetStyle() & WS_VISIBLE;
		}

		void AbstractWindow::SetVisible(bool newVisibleState)
		{
			DWORD dw = GetStyle() & ~WS_VISIBLE;
			if (newVisibleState)
				dw |= WS_VISIBLE;
			SetStyle(dw);

			if (this->CanProvideParent()) {
				for (int idx = 0; idx < GetChildCount(); idx++) {
					AbstractWindow* pChild = GetChild(idx);
					if (pChild)
						pChild->Invalidate();
				}
			}

			Invalidate();

			if (GetParent()) {
				GetParent()->UpdateLayout();
				GetParent()->Invalidate();
			}
		}

		bool AbstractWindow::IsEnable() {
			return !(GetStyle() & WS_DISABLED);
		}

		void AbstractWindow::SetEnable(bool newEnableState)
		{
			EnableWindow(GetHandle(), newEnableState);

			if (this->CanProvideParent()) {
				for (int idx = 0; idx < GetChildCount(); idx++) {
					AbstractWindow* pChild = GetChild(idx);
					if (pChild)
						pChild->SetEnable(newEnableState);
				}
			}
		}

		void AbstractWindow::Invalidate() {
			InvalidateRect(GetHandle(), NULL, TRUE);
		}

		void AbstractWindow::Update() {
			UpdateWindow(hWnd);
			UpdateLayout();
		}

		AbstractWindow::AbstractWindow(HINSTANCE hInstance)
		{
			this->pChildList = NULL;
			this->pStdWindowProc = NULL;
			this->pLayoutProvider = NULL;
			this->pParentWindow = NULL;

			this->hInstance = hInstance;
		}

		AbstractWindow::~AbstractWindow() 
		{
			if (pLayoutProvider)
				delete pLayoutProvider;

			if (pChildList) {
				if (CanProvideParent()) {
					for (auto it = pChildList->begin(); it != pChildList->end(); it++)
						delete *it;
				}
				pChildList->clear();
				delete pChildList;
			}

			UnRegisterWindow(hWnd);
			DestroyWindow(hWnd);
		}
	}
}