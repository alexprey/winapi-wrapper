///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)
///	AbstractWindow.h
///		Core window class - abstract window class

#ifndef __ABSTRACT_WINDOW_H
#define __ABSTRACT_WINDOW_H

#include <Windows.h>
#include <tchar.h>
#include <vector>
#include <unordered_map>

#include <AWF\Misc\Container.h>
#include <AWF\Misc\Rect.h>
#include <AWF\Misc\ILayoutProvider.h>

using namespace AWF::Misc;
using namespace std;

namespace AWF
{
	namespace Core
	{

		enum HandlerFlags {
			// Notify handling system about you handled this message in your function. This stop custom handling flow process
			HANDLER_FLAG_HANDLED		= 1,
			// After you handler function calling default system handle procedure "WNDPROC"
			HANDLER_FLAG_USE_DEFAULT	= 2,
			// Return you value in global handling flow process
			HANDLER_FLAG_RETURN_HANDLED = 4, 
		};

		// Handling flow process diagram
		//
		//	CustomWndProc(...) -> YourHandlers[...](...)
		//							if (use HANDLER_FLAG_USE_DEFAULT)
		//								StdControlWndProc(...)
		//								if (use HANDLER_FLAG_RETURN_HANDLED)
		//									return YoutHandlers_returnValue[...]
		//								else
		//									return StdControlWndProc_returnValue

		// Base window class
		class AbstractWindow;
		
		// Message handler function declaration
		typedef LRESULT (*WinMessageHandlerFunction)(AbstractWindow*, const MSG&, DWORD& hfFlags);
		// Lambda delaration for quick use lamda statement in message handling system
		#define WinMessageHandlerFunction_expr [](AbstractWindow* pSender, const MSG& msg, DWORD& hfFlags) -> LRESULT

		// Message handler table
		typedef unordered_map<UINT, vector<WinMessageHandlerFunction>*> WinMessageHandlerTable;

		// Base window class
		class AbstractWindow
		{
		public:
			// Find base window class in global window table
			static AbstractWindow* GetWindow(HWND hWnd);
		private:
			static WNDCLASSEX _StdFormWindowClass;
			static bool _IsInitWindowClass;
			static void _InitWindowClass();

			static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
			static void RegisterWindow(HWND hWnd, AbstractWindow* pWindow);
			static void UnRegisterWindow(HWND hWnd);
		private:
			WNDPROC pStdWindowProc;
			HINSTANCE hInstance;
			HWND hWnd;

			AbstractWindow* pParentWindow;
			Rect rect;
			Container container;
			ILayoutProvider* pLayoutProvider;

			vector<AbstractWindow*>* pChildList;
			WinMessageHandlerTable winMsgHandlerTable;
			string name;

			LRESULT ProcedHandlers(UINT msg, WPARAM wParam, LPARAM lParam, DWORD& hfFlags);
		protected:
			// Callback method, invoked when window resizing
			virtual void OnResize(int width, int height);
			// Callback method, invoked after window creation
			virtual void OnCreate() {}
		public:
			AbstractWindow(HINSTANCE hInstance);
			virtual ~AbstractWindow();

			// Get currect window style
			DWORD GetStyle();
			// Set new window style
			void SetStyle(DWORD newStyle);

			// -- Initialize --
#pragma region

			// Get default window styles
			virtual DWORD GetDefaultStyle();
			// Get default window extension styles
			virtual DWORD GetDefaultStyleEx();
			// Initialize window class
			HRESULT Initialize(int positionX, int positionY, int width, int height, DWORD dwWindowStyle = NULL, DWORD dwExWindowStyle = NULL, AbstractWindow* pParent = NULL, const LPWSTR szClassName = NULL);

#pragma endregion

			// -- Parent \ Child --
#pragma region
			
			// TRUE when this window can containt childs parented
			virtual bool CanProvideParent() { return true; }

			// Return parent window
			AbstractWindow* GetParent();
			// Set new parent window
			virtual void SetParent(AbstractWindow* pNewParent);

			// Return child count on this window
			int GetChildCount();
			// Return child window with IDX
			AbstractWindow* GetChild(int idx);

			// Add new child on this window
			virtual void AddChild(AbstractWindow* pWindow);
			// Delete child from this window
			virtual void DeleteChild(AbstractWindow* pWindow);

#pragma endregion

			// -- Container --
#pragma region

			// Return window rectangle data
			virtual void GetRect(Rect* pRect);
			// Set new window rectangle without container info
			virtual void ApplyRect(Rect* pRect);
			// Set new window rectangle with container info
			virtual void ApplyMargedRect(Rect* pRect);
	
			// Set new window position
			virtual void SetPosition(int newPositionX, int newPositionY);
			// Return window X position
			virtual int GetPositionX();
			// Return window Y position
			virtual int GetPositionY();

			// Set new window size
			virtual void SetSize(int newWidth, int newHeight);
			// Return window width
			virtual int GetWidth();
			// Return window height
			virtual int GetHeight();

#pragma endregion

			// -- Window Container --
#pragma region

			// Return window container width
			virtual int GetContainerWidth();
			// Return window container height
			virtual int GetContainerHeight();

			// Set window container margine on rigt and bottom side
			virtual void SetMargin(int newMarginRight, int newMarginBottom);
			// Return right window container margin
			virtual int GetMarginRight();
			// Return bottom window container margin
			virtual int GetMarginBottom();

			// Set new window container flags
			virtual void SetContainerFlags(char flags);
			// Return window container flags
			virtual char GetContainerFlags();
#pragma endregion

			// -- Styles --
#pragma region
			// Is window visible
			bool IsVisible();
			void SetVisible(bool newVisibleState);

			// Is window enable
			bool IsEnable();
			void SetEnable(bool newEnableState);
#pragma endregion

			// -- Layout --
#pragma region
			// Set new layout provider on window
			virtual void SetLayoutProvider(ILayoutProvider* pLayoutProvider);
			// Get window layout provider
			virtual ILayoutProvider* GetLayoutProvider();
			// Update full window layout
			virtual void UpdateLayout();
			// Update single container on window wit current window layout provider. If layout provider implement only full recomputing then invok UpdateLayout()
			virtual bool UpdateLayout(const Container* pContainer, Rect* pWindowRect);
			// Update currect window rectangle
			virtual void UpdateRect();
#pragma endregion
			
			// -- Misc --
#pragma region
			// Full window update
			virtual void Update();
			// WinApi::ShowWindow(...)
			virtual void Show(int show);
			// WinApi::InvalidateRect(...)
			virtual void Invalidate();
			// Return window handle
			HWND GetHandle() { return hWnd; };
#pragma endregion

			// -- Message handling --
#pragma region
			// Register new message handler for this window. Return TRUE when success
			bool RegisterMessageHandler(UINT message, WinMessageHandlerFunction pHandler);
			// Delete old message handler for this window. Return TRUE when success
			bool UnRegisterMessageHandler(UINT message, WinMessageHandlerFunction pHandler);
			// Delete all message handlers for this window
			void UnRegisterMessageHandler(UINT message);

#pragma endregion
		};
	}
}

#endif