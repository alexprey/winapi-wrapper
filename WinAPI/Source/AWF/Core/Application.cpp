#include "Application.h"

#include <strsafe.h>

namespace AWF
{
	namespace Core
	{
		wstring Application::GetErrorMessage(DWORD dwErrorCode)
		{
			// Retrieve the system error message for the last-error code
			LPVOID lpMsgBuf;
			FormatMessage(
				FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM |
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				dwErrorCode,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				(LPTSTR) &lpMsgBuf,
				0, NULL );

			return wstring ((LPTSTR)lpMsgBuf);
		}

		void Application::ErrorExit(LPTSTR lpszFunction) 
		{ 
			// Retrieve the system error message for the last-error code

			LPVOID lpMsgBuf;
			LPVOID lpDisplayBuf;
			DWORD dw = GetLastError(); 

			FormatMessage(
				FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM |
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				dw,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				(LPTSTR) &lpMsgBuf,
				0, NULL );

			// Display the error message and exit the process

			lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, 
				(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR)); 
			StringCchPrintf((LPTSTR)lpDisplayBuf, 
				LocalSize(lpDisplayBuf) / sizeof(TCHAR),
				TEXT("%s failed with error %d: %s"), 
				lpszFunction, dw, lpMsgBuf); 
			MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK); 

			LocalFree(lpMsgBuf);
			LocalFree(lpDisplayBuf);
			ExitProcess(dw);
		}

		Application::Application(HINSTANCE hInstance, HINSTANCE hPrevInstance) 
		{
			this->hInstance = hInstance;
			this->hPrevInstance = hPrevInstance;
			pMainForm = NULL;
			isNeedTerminate = false;
		}

		void Application::Terminate() 
		{ 
			isNeedTerminate = true; 
		}

		bool Application::IsTerminated() 
		{ 
			return isNeedTerminate; 
		}

		void Application::ProcessMessages()
		{
			int maxCount = 50;
			MSG msg = {0};
			while (GetMessage(&msg, NULL, 0, 0) & maxCount-- > 0) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		void Application::Run()
		{
			MSG msg = {0};
			while (GetMessage(&msg, NULL, 0, 0) && !isNeedTerminate) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		UIForm* Application::GetMainForm()
		{
			return pMainForm;
		}

		void Application::SetMainForm(UIForm* pWindow) 
		{
			pMainForm = pWindow;
		}
	}
}