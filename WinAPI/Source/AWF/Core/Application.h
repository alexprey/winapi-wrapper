///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)
///	Application.h
///		Application class

#ifndef __APPLICATION_H
#define __APPLICATION_H

#include "AbstractWindow.h"
#include <AWF\Controls\UIForm.h>

using namespace AWF::Controls;

namespace AWF
{
	namespace Core
	{
		class Application
		{
		private:
			UIForm* pMainForm;

			HINSTANCE hInstance;
			HINSTANCE hPrevInstance;
			bool isNeedTerminate;
		public:
			Application::Application(HINSTANCE hInstance, HINSTANCE hPrevInstance);

			void Terminate();
			bool IsTerminated();

			void ProcessMessages();

			void Run();
			void SetMainForm(UIForm* pWindow);
			UIForm* GetMainForm();

			HINSTANCE GetInstance() { return hInstance; }

			static wstring GetErrorMessage(DWORD dwErrorCode);
			static void ErrorExit(LPTSTR lpszFunction);
		};
	}
}

#endif