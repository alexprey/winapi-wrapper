///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)
///	SimpleControl.h
///		Simple UI Control without parent provider

#ifndef __SIMPLE_CONTROL_H
#define __SIMPLE_CONTROL_H

#include "AbstractWindow.h"

namespace AWF
{
	namespace Core
	{
		class SimpleControl : public AbstractWindow
		{
		public:
			SimpleControl(HINSTANCE hInstance) : AbstractWindow(hInstance) {}
			virtual ~SimpleControl() {};

			bool CanProvideParent() { return false; }
		};
	}
}

#endif