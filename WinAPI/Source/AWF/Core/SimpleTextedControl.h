///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)
///	SimpleTextedControl.h
///		Simple UI Control without parent provider based on TextedAbstractWindow

#ifndef __SIMPLE_TEXTED_CONTROL_H
#define __SIMPLE_TEXTED_CONTROL_H

#include "TextedAbstractWindow.h"

namespace AWF
{
	namespace Core
	{
		class SimpleTextedControl : public TextedAbstractWindow
		{
		public:
			SimpleTextedControl(HINSTANCE hInstance) : TextedAbstractWindow(hInstance) {}
			virtual ~SimpleTextedControl() {};

			bool CanProvideParent() { return false; }
		};
	}
}

#endif