#include "TextedAbstractWindow.h"

namespace AWF
{
	namespace Core
	{
		void TextedAbstractWindow::SetText(wstring newText)
		{
			SetWindowText(this->GetHandle(), newText.c_str());
		}

		wstring TextedAbstractWindow::GetText()
		{
			LPWSTR buffer = new TCHAR[255];
			GetWindowText(this->GetHandle(), buffer, 255);
			wstring result(buffer);
			delete[] buffer;
			return result;
		}
	}
}