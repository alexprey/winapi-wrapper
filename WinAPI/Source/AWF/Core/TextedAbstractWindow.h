///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)
///	TextedAbstractWindow.h
///		Window with Set\Get Text methods

#ifndef __TEXTED_ABSTRACT_WINDOW_H
#define __TEXTED_ABSTRACT_WINDOW_H

#include "AbstractWindow.h"

namespace AWF
{
	namespace Core
	{
		class TextedAbstractWindow : public AbstractWindow
		{
		public:
			TextedAbstractWindow(HINSTANCE hInstance) : AbstractWindow(hInstance) {}
			virtual ~TextedAbstractWindow() {}

			virtual void SetText(wstring newText);
			virtual wstring GetText();
		};
	}
}

#endif