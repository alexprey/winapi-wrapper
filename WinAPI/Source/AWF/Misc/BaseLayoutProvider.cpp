#include "BaseLayoutProvider.h"

namespace AWF
{
	namespace Misc
	{
		void BaseLayoutProvider::SetWidth(int value) { this->_width = value; }
		int BaseLayoutProvider::GetWidth() { return this->_width; }

		void BaseLayoutProvider::SetHeight(int value) { this->_height = value; }
		int BaseLayoutProvider::GetHeight() { return this->_height; }

		void BaseLayoutProvider::SetLeftWidth(int value) { this->_leftWidth = value; }
		int BaseLayoutProvider::GetLeftWidth() { return this->_leftWidth; }

		void BaseLayoutProvider::SetRightWidth(int value) { this->_rightWidth = value; }
		int BaseLayoutProvider::GetRightWidth() { return this->_rightWidth; }

		void BaseLayoutProvider::SetTopHeight(int value) { this->_topHeight = value; }
		int BaseLayoutProvider::GetTopHeight() { return this->_topHeight; }

		void BaseLayoutProvider::SetBottomHeight(int value) { this->_bottomHeight = value; }
		int BaseLayoutProvider::GetBottomHeight() { return this->_bottomHeight; }

		void BaseLayoutProvider::SetChildVector(void* pChildList) { this->_pChildList = pChildList; }

		void BaseLayoutProvider::SetMargin(int left, int top, int right, int bottom)
		{
			SetLeftWidth(left);
			SetTopHeight(top);
			SetRightWidth(right);
			SetBottomHeight(bottom);
		}

		vector<AbstractWindow*>* BaseLayoutProvider::GetChildVector() {
			return (vector<AbstractWindow*>*)_pChildList;
		}
	}
}