///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __BASE_LAYOUT_PROVIDER_H
#define __BASE_LAYOUT_PROVIDER_H

#include "ILayoutProvider.h"
#include <AWF\Core\AbstractWindow.h>

using namespace AWF::Core;

namespace AWF
{
	namespace Misc
	{
		
		class BaseLayoutProvider : public ILayoutProvider
		{
		private:
			int _width;
			int _height;
			int _leftWidth;
			int _rightWidth;
			int _topHeight;
			int _bottomHeight;

			void* _pChildList;
		protected:
			vector<AbstractWindow*>* GetChildVector();
		public:
			BaseLayoutProvider()
			{
				_width = 0;
				_height = 0;
				_leftWidth = 0;
				_rightWidth = 0;
				_topHeight = 0;
				_bottomHeight = 0;
				_pChildList = nullptr;
			}

			void SetWidth(int value);
			int GetWidth();
			void SetHeight(int value);
			int GetHeight();
			void SetLeftWidth(int value);
			int GetLeftWidth();
			void SetRightWidth(int value);
			int GetRightWidth();
			void SetTopHeight(int value);
			int GetTopHeight();
			void SetBottomHeight(int value);
			int GetBottomHeight();

			void SetMargin(int left, int top, int right, int bottom);

			void SetChildVector(void* pChildList);
		};

	}
}

#endif