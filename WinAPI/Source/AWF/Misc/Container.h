///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __CONTAINER_H
#define __CONTAINER_H

namespace AWF
{
	namespace Misc
	{
		const int CF_FIX_LEFT		= 0x1;
		const int CF_FIX_RIGHT		= 0x2;
		const int CF_FIX_TOP		= 0x4;
		const int CF_FIX_BOTTOM		= 0x8;
		const int CF_FIX_LEFT_RIGHT	= CF_FIX_LEFT | CF_FIX_RIGHT;
		const int CF_FIX_TOP_BOTTOM	= CF_FIX_TOP | CF_FIX_BOTTOM;
		const int CF_FIX_ALL		= CF_FIX_TOP_BOTTOM | CF_FIX_LEFT_RIGHT;

		struct Container
		{
			int marginLeft;
			int marginRight;
			int marginTop;
			int marginBottom;
			int width;
			int height;
			char flags;

			Container() {
				marginLeft = 0;
				marginRight = 0;
				marginTop = 0;
				marginBottom = 0;
				width = 0;
				height = 0;
				flags = 0;
			}

			void SetLeftTop(int positionX, int positionY, int width, int height) {
				this->marginLeft = positionX;
				this->marginTop = positionY;
				this->width = width;
				this->height = height;
				this->flags = CF_FIX_LEFT | CF_FIX_TOP;
			}

			void SetMargins(int marginLeft, int marginTop, int marginRight, int marginBottom)
			{
				this->marginLeft = marginLeft;
				this->marginTop = marginTop;
				this->marginRight = marginRight;
				this->marginBottom = marginBottom;
				this->flags = CF_FIX_ALL;
			}
		};
	}
}

#endif