///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __IDATA_PROVIDER_H
#define __IDATA_PROVIDER_H

#include <string>

using namespace std;

namespace AWF
{
	namespace Misc
	{
		struct IDataProvider
		{
			virtual bool IsHasProperty(wstring propertyName) = 0;
			virtual wstring GetProperty(wstring propertyName) = 0;
		};
	}
}

#endif