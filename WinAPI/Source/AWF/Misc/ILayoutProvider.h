///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __ILAYOUT_PROVIDER_H
#define __ILAYOUT_PROVIDER_H

#include "Container.h"
#include "Rect.h"

namespace AWF
{
	namespace Misc
	{

		class ILayoutProvider
		{
		public:
			virtual ~ILayoutProvider() {};

			virtual bool IsFullRecompute() = 0;

			virtual void SetWidth(int value) = 0;
			virtual int GetWidth() = 0;
			virtual void SetHeight(int value) = 0;
			virtual int GetHeight() = 0;
			virtual void SetLeftWidth(int value) = 0;
			virtual int GetLeftWidth() = 0;
			virtual void SetRightWidth(int value) = 0;
			virtual int GetRightWidth() = 0;
			virtual void SetTopHeight(int value) = 0;
			virtual int GetTopHeight() = 0;
			virtual void SetBottomHeight(int value) = 0;
			virtual int GetBottomHeight() = 0;

			// Call SetLeftWidth, SetTopHeight, SetRightWidth and SetBottomHeight with currect arguments
			virtual void SetMargin(int left, int top, int right, int bottom) = 0;

			virtual void SetChildVector(void* pChildList) = 0;
			
			virtual void RecomputeContainer() = 0;
			virtual void RecomputeContainer(const Container* pContainer, Rect* pWindowRect) = 0;
		};
	}
}

#endif