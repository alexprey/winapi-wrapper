#include "StackLayout.h"

namespace AWF
{
	namespace Misc
	{
		namespace Layouts
		{
			bool StackLayout::IsFullRecompute()
			{
				return true;
			}

			StackLayout::StackLayout(int padding, bool fitLast, StackOrientation orientation) : BaseLayoutProvider()
			{
				this->padding = padding;
				this->fitLast = fitLast;
				this->orientation = orientation;
			}

			StackLayout::~StackLayout()
			{
			}

			void StackLayout::RecomputeContainer()
			{
				vector<AbstractWindow*>* pChildVector = GetChildVector();
				if (pChildVector)
				{
					Rect* pRect = new Rect();
					pRect->left = GetLeftWidth();
					if (orientation == STACK_ORIENTATION_VERTICAL)
					{
						pRect->bottom = GetTopHeight();
						pRect->right = GetWidth() - GetRightWidth();
					}
					else if (orientation == STACK_ORIENTATION_HORIZONTAL)
					{
						pRect->bottom = GetHeight() - GetBottomHeight();
						pRect->top = GetTopHeight();
					}

					int idx = 0;
					int count = 0;
					for (auto it = pChildVector->begin(); it != pChildVector->end(); it++) {
						AbstractWindow* pWindow = *it;
						if (pWindow && pWindow->IsVisible()) {
							count++;
						}
					}

					for (auto it = pChildVector->begin(); it != pChildVector->end(); it++) {
						AbstractWindow* pWindow = *it;
						if (pWindow && pWindow->IsVisible()) {
							if (orientation == STACK_ORIENTATION_VERTICAL)
								pRect->top = pRect->bottom;
							else if (orientation == STACK_ORIENTATION_HORIZONTAL)
								pRect->left = pRect->right;

							if (idx > 0) {
								if (orientation == STACK_ORIENTATION_VERTICAL)
									pRect->top += padding;
								else if (orientation == STACK_ORIENTATION_HORIZONTAL)
									pRect->left += padding;
							}

							if ((idx != (count - 1)) || !fitLast)
							{
								if (orientation == STACK_ORIENTATION_VERTICAL)
									pRect->bottom = pRect->top + pWindow->GetContainerHeight();
								else if (orientation == STACK_ORIENTATION_HORIZONTAL)
									pRect->right = pRect->left + pWindow->GetContainerWidth();
							}
							else
							{
								if (orientation == STACK_ORIENTATION_VERTICAL)
									pRect->bottom = GetHeight() - GetBottomHeight();
								else if (orientation == STACK_ORIENTATION_HORIZONTAL)
									pRect->right = GetWidth() - GetRightWidth();
							}
						
							pWindow->ApplyRect(pRect);
							idx++;
						}
					}
					delete pRect;
				}
			}

			void StackLayout::RecomputeContainer(const Container* pContainer, Rect* pWindowRect) {}
		}
	}
}