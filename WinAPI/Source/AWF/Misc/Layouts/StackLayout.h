///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __STACK_LAYOUT_H
#define __STACK_LAYOUT_H

#include <AWF\Misc\BaseLayoutProvider.h>

namespace AWF
{
	namespace Misc
	{
		namespace Layouts
		{

			enum StackOrientation {
				STACK_ORIENTATION_HORIZONTAL,
				STACK_ORIENTATION_VERTICAL,
			};

			//Provide stack elemnt positioning. Top to Down. Element fit all available space on parent window. Last child use all free height on the parent window
			//You can use padding for delimiting objects with free space.
			class StackLayout : public BaseLayoutProvider
			{
			protected:
				int padding;
				StackOrientation orientation;
				bool fitLast;
			public:
				StackLayout(int padding = 0, bool fitLast = false, StackOrientation orientation = STACK_ORIENTATION_VERTICAL);
				virtual ~StackLayout();

				bool IsFullRecompute();

				virtual void RecomputeContainer();
				virtual void RecomputeContainer(const Container* pContainer, Rect* pWindowRect);
			};

		}
	}
}


#endif