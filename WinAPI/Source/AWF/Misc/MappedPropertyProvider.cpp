#include "MappedPropertyProvider.h"

namespace AWF
{
	namespace Misc
	{


		wstring MappedPropertyProvider::GetProperty(wstring propertyName)
		{
			if (!isPropertyDefined) {
				DefiningProperty();
				isPropertyDefined = true;
			}

			PropertyGetter pGetterFunction = propertyMap[propertyName];
			if (pGetterFunction)
				return pGetterFunction();

			return L"";
		}

		bool MappedPropertyProvider::IsHasProperty(wstring propertyName)
		{
			return propertyMap[propertyName] != NULL;
		}
	}
}