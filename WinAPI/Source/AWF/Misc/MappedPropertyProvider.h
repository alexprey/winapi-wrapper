///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __MAPPED_PROPERTY_PROVIDER_H
#define __MAPPED_PROPERTY_PROVIDER_H

#include <string>
#include <unordered_map>
#include "IDataProvider.h"

using namespace std;

namespace AWF
{
	namespace Misc
	{
		typedef wstring (*PropertyGetter) ();
		#define PropertyGetter_expr []() -> wstring
		//#define DefineProperty(propertyName, propertyGetter) propertyMap[propertyName] = PropertyGetter_expr {propertygetter};

		class MappedPropertyProvider : public IDataProvider
		{
		private:
			bool isPropertyDefined;
		protected:
			unordered_map<wstring, PropertyGetter> propertyMap;

			virtual void DefiningProperty() = 0;
		public:
			MappedPropertyProvider() { isPropertyDefined = false; }

			virtual bool IsHasProperty(wstring propertyName);
			virtual wstring GetProperty(wstring propertyName);
		};
	}
}

#endif