///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __RECT_H
#define __RECT_H

#include <AWF\Utility\BaseUtility.h>

using namespace AWF::Utility;

namespace AWF
{
	namespace Misc
	{
		struct Rect
		{
			int left;
			int top;
			int right;
			int bottom;

			Rect()
			{
				this->left = 0;
				this->right = 0;
				this->top = 0;
				this->bottom = 0;
			}

			Rect(int left, int top, int right, int bottom)
			{
				this->left = left;
				this->right = right;
				this->top = top;
				this->bottom = bottom;
			}

			int GetWidth()
			{
				return this->right - this->left;
			}

			int GetHeight()
			{
				return this->bottom - this->top;
			}

			void Clip(const Rect* pRect)
			{
				if (pRect)
				{
					this->left = Max(this->left, pRect->left);
					this->right = Min(this->right, pRect->right);
					this->top = Max(this->top, pRect->top);
					this->bottom = Min(this->bottom, pRect->bottom);
				}
			}
		};
		
	}
}

#endif