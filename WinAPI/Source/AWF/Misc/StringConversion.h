#ifndef SC_STRING_CONVERSIONS_H
#define SC_STRING_CONVERSIONS_H

#include <string>
#include <sstream>
#include <ios>

namespace AWF
{
	namespace Misc
	{
		/// ===== string =====
		template <typename T>
		std::string toString(T val, const std::ios_base::fmtflags & formatFlag = std::ios_base::dec)
		{
			std::ostringstream z;
			std::ios_base & zi = z;
			zi.setf(formatFlag);
			z << val;
			zi.unsetf(formatFlag);
			return z.str();
		}

		template<typename T> 
		T fromString(const std::string& s) 
		{
		  std::istringstream z(s);
		  T result;
		  z >> result;
		  return result;
		}

		/// ===== wstring =====
		template <typename T>
		std::wstring toWString(T val, const std::ios_base::fmtflags & formatFlag = std::ios_base::dec)
		{
			std::wostringstream z;
			std::ios_base & zi = z;
			zi.setf(formatFlag);
			z << val;
			zi.unsetf(formatFlag);
			return z.str();
		}

		template<typename T> 
		T fromWString(const std::wstring& s) 
		{
		  std::wistringstream z(s);
		  T result;
		  z >> result;
		  return result;
		}

		template <typename T>

		/// ===== Unicode-conditional string =====
		#ifdef UNICODE
		std::wstring toTString(T val, const std::ios_base::fmtflags & formatFlag = std::ios_base::dec) {
			return toWString(val,formatFlag);
		}
		template<typename T> 
		T fromTString(const std::wstring& s)  {
			return fromWString(s);
		}
		#else
		std::string toTString(T val, const std::ios_base::fmtflags & formatFlag = std::ios_base::dec) {
			return toString(val,formatFlag);
		}
		template<typename T>
		T fromTString(const std::string& s)  {
			return fromString(s);
		}
		#endif
	}
}

#endif
