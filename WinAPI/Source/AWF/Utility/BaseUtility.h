///	AlexPrey Win Framework
///		by Alexprey - 2013 (c)

#ifndef __BASE_UTILITY_H
#define __BASE_UTILITY_H

namespace AWF
{
	namespace Utility
	{

		template <typename T>
		T Min(T val1, T val2) { return (val1 <= val2) ? val1 : val2; }

		template <typename T>
		T Max(T val1, T val2) { return (val1 >= val2) ? val1 : val2; }

	}
}

#endif