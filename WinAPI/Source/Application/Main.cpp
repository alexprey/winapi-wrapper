#ifdef UNICODE
	#ifndef _UNICODE
		#define _UNICODE
	#endif
#endif

#include <tchar.h>
#include <vector>
#include <windows.h>
#include <TlHelp32.h>
#include <Psapi.h>

#include <cfgmgr32.h>
#include <devguid.h>
#include <SetupAPI.h>
#include <Dbt.h>

#pragma comment(linker, "/manifestdependency:\"type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' \
language='*'\"")
#include <CommCtrl.h>

//	Custom includes

#include <AWF\AWF.h>
#include <AWF\Core\AbstractWindow.h>
#include <AWF\BasicControls.h>
#include <AWF\Controls\UIListView.h>
#include <AWF\Misc\MappedPropertyProvider.h>
#include <AWF\Misc\StringConversion.h>
#include <AWF\Misc\Layouts\StackLayout.h>

using namespace std;
using namespace AWF;
using namespace AWF::Core;
using namespace AWF::Misc;
using namespace AWF::Misc::Layouts;

UIForm* pMainWindow;
wstring FormTitle = L"AP ProcessManager";

wstring memoryMetrics[] = {L"B", L"KB", L"MB", L"GB"};
wstring bytesToMetrics(DWORD bytes)
{
	wstring result;
	double value = bytes;
	int idx = 0;

	do
	{
		result = toWString(((int)(value * 10) / 10.0)) + L" " + memoryMetrics[idx++];
		value /= 1024.0;
	} while (value > 0.8 && (idx < 4));

	return result;
}

class ProcessInfo : public IDataProvider
{
public:
	DWORD processId;
	wstring ProcessName;
	DWORD ThreadCount;
	SIZE_T memoryUsage;
	SIZE_T cpuUsage;
public:
	bool IsHasProperty(wstring propertyName)
	{
		return (propertyName == L"ProcessName") ||
				(propertyName == L"CPUUsage") ||
				(propertyName == L"MemoryUsage") ||
				(propertyName == L"ThreadCount") ||
				(propertyName == L"ID");
	}

	wstring GetProperty(wstring propertyName)
	{
		if (propertyName == L"ProcessName")
			return ProcessName;
		else if (propertyName == L"CPUUsage")
			return toWString(cpuUsage);
		else if (propertyName == L"MemoryUsage")
			return bytesToMetrics(memoryUsage);// toWString(memoryUsage);
		else if (propertyName == L"ID")
			return toWString(processId);
		else if (propertyName == L"ThreadCount")
			return toWString(ThreadCount);

		return L"";
	}
public:
	ProcessInfo(wstring procName) {
		ProcessName = procName;
		ThreadCount = 0;
		cpuUsage = 0;
		memoryUsage = 0;
	}
};

class ThreadInfo : public IDataProvider
{
public:
	DWORD threadId;
	DWORD priority;
public:
	bool IsHasProperty(wstring propertyName)
	{
		return (propertyName == L"ID") ||
				(propertyName == L"Priority");
	}

	wstring GetProperty(wstring propertyName)
	{
		if (propertyName == L"ID")
			return toWString(threadId);
		else if (propertyName == L"Priority")
			return toWString(priority);
		return L"";
	}
public:
	ThreadInfo(DWORD id, DWORD pri) {
		this->threadId = id;
		this->priority = pri;
	}
};

class DeviceInfo : public IDataProvider
{
public:
	wstring name;
public:
	bool IsHasProperty(wstring propertyName)
	{
		return (propertyName == L"Name");
	}

	wstring GetProperty(wstring propertyName)
	{
		if (propertyName == L"Name")
			return name;
		return L"";
	}
public:
	DeviceInfo(wstring Name) {
		this->name = Name;
	}
};

vector<ProcessInfo*> processList;
vector<ThreadInfo*> threadList;
vector<DeviceInfo*> deviceList;
UITextEdit* pEdit;
UIButton* pCloseButton;

UIButton* pChangePanels;

UIPanel* pProcessPanel;
	UIListView* pListView;
	UIPanel* pProcessPanelInfo;
		UILabel* pPPInfo_Name;
		UILabel* pPPInfo_ThreadCount;
		UIListView* pPPInfo_Threads;
UIPanel* pDevicePanel;
	UIListView* pDeviceListView;
	UIPanel* pDevicePanelInfo;
		UILabel* pDPInfo_Name;

void UpdateThreadList(DWORD processId)
{
	for (auto it = threadList.begin(); it != threadList.end(); it++)
		delete *it;
	threadList.clear();

	HANDLE const hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, processId);
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		THREADENTRY32 teThreadEntry;
		teThreadEntry.dwSize = sizeof(THREADENTRY32);
		if (Thread32First(hSnapshot, &teThreadEntry))
		{
			do {
				if (teThreadEntry.th32OwnerProcessID == processId) {
					ThreadInfo* pThreadInfo = new ThreadInfo(teThreadEntry.th32ThreadID, teThreadEntry.tpBasePri);
					threadList.insert(threadList.end(), pThreadInfo);
				}
			} while (Thread32Next(hSnapshot, &teThreadEntry));
		}

		CloseHandle(hSnapshot);
	}

	if (pPPInfo_Threads) {
		pPPInfo_Threads->BindData((vector<IDataProvider*>*)&threadList);
		pPPInfo_Threads->Update();
	}
}

void UpdateProcessList()
{
	for(auto it = processList.begin(); it != processList.end(); it++)
		delete *it;
	processList.clear();

	HANDLE const hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		PROCESSENTRY32 peProcessEntry;
		peProcessEntry.dwSize = sizeof(PROCESSENTRY32);
		if (Process32First(hSnapshot, &peProcessEntry))
		{
			do {
				ProcessInfo* pProcessInfo = new ProcessInfo(peProcessEntry.szExeFile);
				processList.insert(processList.begin(), pProcessInfo);
				pProcessInfo->processId = peProcessEntry.th32ProcessID;
				pProcessInfo->ThreadCount = peProcessEntry.cntThreads;

				HANDLE pHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,  false, peProcessEntry.th32ProcessID);
				if (pHandle != INVALID_HANDLE_VALUE) {
					PROCESS_MEMORY_COUNTERS procMemory;
					procMemory.cb = sizeof(PROCESS_MEMORY_COUNTERS);
					if (GetProcessMemoryInfo(pHandle, &procMemory, sizeof(PROCESS_MEMORY_COUNTERS))) {
						pProcessInfo->memoryUsage = procMemory.PagefileUsage;
					}
					
					CloseHandle(pHandle);
				}
			} while (Process32Next(hSnapshot, &peProcessEntry));
		}

		CloseHandle(hSnapshot);
	}

	if (pListView)
	{
		pListView->BindData((vector<IDataProvider*>*)&processList);
		pListView->UpdateBindedData();
	}
}

void UpdateDeviceList()
{
	for(auto it = deviceList.begin(); it != deviceList.end(); it++)
		delete *it;
	deviceList.clear();

	HDEVINFO hDevInfo;
	SP_DEVINFO_DATA DeviceInfoData;

	hDevInfo = SetupDiGetClassDevs(NULL, 0, 0, DIGCF_PRESENT | DIGCF_ALLCLASSES);
	if (hDevInfo == INVALID_HANDLE_VALUE)
		return;

	DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	for (DWORD i = 0; SetupDiEnumDeviceInfo(hDevInfo, i, &DeviceInfoData); i++)
	{
		DWORD DataT;
		LPTSTR buffer = new TCHAR[512];
		DWORD bufferSize = 512;
		while (!SetupDiGetDeviceRegistryProperty(
			hDevInfo,
			&DeviceInfoData,
			SPDRP_DEVICEDESC,
			&DataT,
			(PBYTE)buffer,
			bufferSize,
			&bufferSize))
		{
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
				if (buffer) delete[] buffer;
				bufferSize *= 2;
				buffer = new TCHAR[bufferSize];
			}
		}

		if (buffer) {
			DeviceInfo* pDeviceInfo = new DeviceInfo(buffer);
			deviceList.insert(deviceList.end(), pDeviceInfo);
			delete[] buffer;
		}
	}

	SetupDiDestroyDeviceInfoList(hDevInfo);

	if (pDeviceListView)
		pDeviceListView->UpdateBindedData();
}

//=======================================================================

//=======================================================================

LRESULT ListViewOnSelect(AbstractWindow* pWindow, const MSG& msg, DWORD& hfFlags)
{
	int selected = pListView->GetFirstSelectItem();
	pCloseButton->SetEnable(selected >= 0);
	return 0;
}

LRESULT ListViewNotify(AbstractWindow* pWindow, const MSG& msg, DWORD& hfFlags)
{
	NMLVDISPINFO* plvdi;
	AbstractWindow* pSender = AbstractWindow::GetWindow(((LPNMHDR)msg.lParam)->hwndFrom);
	if (pSender == pListView)
	{
		switch (((LPNMHDR)msg.lParam)->code)
		{
		case NM_CLICK:
			{
				hfFlags &= ~HANDLER_FLAG_USE_DEFAULT;
				hfFlags |= HANDLER_FLAG_HANDLED;
				return ListViewOnSelect(pWindow, msg, hfFlags);
			} break;
		}
	}

	return 0;
}

LRESULT OnCloseButtonClick(AbstractWindow* pWindow, const MSG& msg, DWORD& hfFlags)
{
	static bool terminating = false;

	if (msg.message == WM_KEYDOWN)
		if (msg.wParam != VK_DELETE)
			return 0;

	int selected = pListView->GetFirstSelectItem();
	if (selected >= 0) {
		ProcessInfo* pProcessInfo = processList[selected];
		if (pProcessInfo) {
			if (terminating) {
				MessageBox(NULL, L"You can not close new process until terminating other process", L"Warning", MB_OK | MB_ICONWARNING);
				return 0;
			}

			wstring msg = L"Close \"" + pProcessInfo->ProcessName + L"\" process?";
			if (MessageBox(NULL, (wchar_t*)msg.c_str(), L"Confirmation", MB_YESNO | MB_ICONQUESTION) == IDYES)
			{
				HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, false, pProcessInfo->processId);
				if (hProcess) {
					terminating = true;

					wstring animationArray[] = {L".", L"..", L"..."};
					int animationSize = ARRAYSIZE(animationArray);
					int animationStateId = 0;

					if (TerminateProcess(hProcess, 0))
					{
						while (true) {
							DWORD dw = WaitForSingleObject(hProcess, 100);
							if (dw == WAIT_TIMEOUT) {
								pMainWindow->SetText(FormTitle + L" | Closing process \"" + pProcessInfo->ProcessName + L"\" " + animationArray[animationStateId]);
								animationStateId = (animationStateId + 1) % animationSize;
								pApplication->ProcessMessages();
							} else 
								break;
						}
					} else {
						MessageBox(NULL, (wchar_t*)Application::GetErrorMessage(GetLastError()).c_str(), L"Error", MB_OK | MB_ICONERROR);
					}
					CloseHandle(hProcess);

					terminating = false;
					UpdateProcessList();
				} else {
					MessageBox(NULL, (wchar_t*)Application::GetErrorMessage(GetLastError()).c_str(), L"Error", MB_OK | MB_ICONERROR);
				}
			}
		}
	}

	return 0;
}

void UpdateProcessInfoPanel(ProcessInfo* pInfo)
{
	if (pInfo) {
		pPPInfo_Name->SetText(L"Process: " + pInfo->ProcessName);
		pPPInfo_ThreadCount->SetText(L"Thread count: " + pInfo->ThreadCount);
		UpdateThreadList(pInfo->processId);
	}
}

LRESULT OnDblClick(AbstractWindow* pWindow, const MSG& msg, DWORD& hfFlags)
{
	if (msg.message == WM_KEYDOWN)
		if (msg.wParam != VK_RETURN)
			return 0;

	int selected = pListView->GetFirstSelectItem();
	if (selected >= 0) {
		static int lastSelected = -1;
		pProcessPanelInfo->SetVisible( lastSelected != selected ? true : !pProcessPanelInfo->IsVisible());
		lastSelected = selected;

		ProcessInfo* pInfo = processList[selected];
		UpdateProcessInfoPanel(pInfo);
	} else {
		pProcessPanelInfo->SetVisible(false);
	}
	pProcessPanelInfo->GetParent()->UpdateLayout();

	return 0;
}

LRESULT OnButtonClick(AbstractWindow* pWindow, const MSG& msg, DWORD& hfFlags)
{
	wstring procName = pEdit->GetText();
	if (procName.length())
	{
		STARTUPINFO startupInfo;
		PROCESS_INFORMATION processInfo;
		ZeroMemory(&startupInfo, sizeof(STARTUPINFO));
		startupInfo.cb = sizeof(STARTUPINFO);
		startupInfo.dwFlags = STARTF_USESHOWWINDOW;
		startupInfo.wShowWindow = SW_SHOWDEFAULT;
		if (!CreateProcess(NULL, (wchar_t*)procName.c_str(), NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &startupInfo, &processInfo))
		{
			MessageBox(NULL, L"Can not create new process", L"Error", MB_OK | MB_ICONERROR);
			return 0;
		}
		
		CloseHandle(processInfo.hProcess);
		CloseHandle(processInfo.hThread);

		UpdateProcessList();
	}

	return 0;
}

void ShowNotify(wstring notifyMessage)
{
	AbstractWindow* pWindow = new AbstractWindow(NULL);
	pWindow->Initialize(0, -40, 400, 40, WS_POPUP, WS_EX_TOPMOST | WS_EX_TOOLWINDOW, NULL, L"Static");

	UILabel* pLabel = new UILabel(NULL);
	pLabel->Initialize(0, 0, 0, 40, pWindow);
	pLabel->SetStyle(pLabel->GetStyle() | ES_CENTER);
	pLabel->SetContainerFlags(CF_FIX_ALL);
	pLabel->SetText(notifyMessage);

	pWindow->Show(SW_SHOW);

	CreateThread(NULL, NULL, [](LPVOID _arg) -> DWORD {
		AbstractWindow* pWindow = (AbstractWindow*)_arg;
		int dX = (pWindow->GetWidth() * 25) / 1000;
		int dY = ((pWindow->GetHeight() + 20) * 25) / 1000;
		while (pWindow->GetPositionY() < 20) {
			pWindow->SetPosition(pWindow->GetPositionX(), pWindow->GetPositionY() + dY);		
			Sleep(25);
		}

		Sleep(5000);

		while (pWindow->GetPositionX() + pWindow->GetWidth() > 0) {
			pWindow->SetPosition(pWindow->GetPositionX() - dX, pWindow->GetPositionY());
			Sleep(25);
		}

		delete pWindow;
		return 0;
	}, pWindow, NULL, NULL);
}

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	InitCommonControls();
	pApplication = new Application(hInstance, hPrevInstance);

	// Create main window
	pMainWindow = new UIForm(pApplication->GetInstance());
	pMainWindow->Initialize(500, 300, 655, 400);
	pMainWindow->SetText(L"Process manager");
	pMainWindow->RegisterMessageHandler(WM_DEVICECHANGE, WinMessageHandlerFunction_expr
		{
			

			if (msg.wParam == DBT_DEVICEARRIVAL) {
				PDEV_BROADCAST_HDR pHDR = (PDEV_BROADCAST_HDR)msg.lParam;
				wstring sMsg = L"Unknow device type arrival";
				if (pHDR->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE) {
					PDEV_BROADCAST_DEVICEINTERFACE pDevInterface = (PDEV_BROADCAST_DEVICEINTERFACE)msg.lParam;
					sMsg = L"New interface connected to this system: " + pDevInterface->dbcc_name[0];
				} else if (pHDR->dbch_devicetype == DBT_DEVTYP_HANDLE) {
					sMsg = L"New handle conneted to this system";
				} else if (pHDR->dbch_devicetype == DBT_DEVTYP_PORT) {
					sMsg = L"New connection from COM or Serial port to this system";
				} else if (pHDR->dbch_devicetype == DBT_DEVTYP_VOLUME) {
					PDEV_BROADCAST_VOLUME pDevVolume = (PDEV_BROADCAST_VOLUME)msg.lParam;
					wchar_t volumeChar = 'A';
					DWORD dw = pDevVolume->dbcv_unitmask;
					while ((dw & 1) == 0) {
						dw = dw >> 1; volumeChar++;
					}

					sMsg = L"New logic volume (" + toWString(volumeChar) + L":\\) connect to this system";
				}

				ShowNotify(sMsg);
				UpdateDeviceList();
				return TRUE;
			} else if (msg.wParam == DBT_DEVICEREMOVECOMPLETE) {
				PDEV_BROADCAST_HDR pHDR = (PDEV_BROADCAST_HDR)msg.lParam;
				wstring sMsg = L"Unknow device type unconnected";
				if (pHDR->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE) {
					PDEV_BROADCAST_DEVICEINTERFACE pDevInterface = (PDEV_BROADCAST_DEVICEINTERFACE)msg.lParam;
					sMsg = L"Interface unconnected from this system: " + pDevInterface->dbcc_name[0];
				} else if (pHDR->dbch_devicetype == DBT_DEVTYP_HANDLE) {
					sMsg = L"Handle unconneted from this system";
				} else if (pHDR->dbch_devicetype == DBT_DEVTYP_PORT) {
					sMsg = L"Disconnect COM or Serial port from this system";
				} else if (pHDR->dbch_devicetype == DBT_DEVTYP_VOLUME) {
					PDEV_BROADCAST_VOLUME pDevVolume = (PDEV_BROADCAST_VOLUME)msg.lParam;
					wchar_t volumeChar = 'A';
					DWORD dw = pDevVolume->dbcv_unitmask;
					while ((dw & 1) == 0) {
						dw = dw >> 1; volumeChar++;
					}

					sMsg = L"Logic volume (" + toWString(volumeChar) + L":\\) disconnect from this system";
				}

				ShowNotify(sMsg);
				UpdateDeviceList();
				return TRUE;
			}

			return 0;
		});

	// Create layout provider for window
	ILayoutProvider* pStackLayout = new StackLayout(4, true);
	pStackLayout->SetLeftWidth(10);
	pStackLayout->SetRightWidth(10);
	pStackLayout->SetTopHeight(10);
	pStackLayout->SetBottomHeight(10);
	pMainWindow->SetLayoutProvider(pStackLayout);

	// Create Components

	//	Process creating panel
#pragma region
	//		Panel
	UIPanel* pPanel = new UIPanel(NULL);
	pPanel->Initialize(0, 0, 100, 30, NULL, pMainWindow);
	//		CmdLine
	pEdit = new UITextEdit(NULL);
	pEdit->Initialize(2, 2, 100, 25, pPanel);
	pEdit->SetMargin(80, 2);
	pEdit->SetContainerFlags(CF_FIX_LEFT_RIGHT | CF_FIX_TOP);
	pEdit->RegisterMessageHandler(WM_KEYUP, WinMessageHandlerFunction_expr 
		{
			if (msg.wParam == VK_RETURN)
				return OnButtonClick(pSender, msg, hfFlags);

			return 0;
		});
	//		Run button
	UIButton* pRunButton = new UIButton(NULL);
	pRunButton->Initialize(0, 2, 70, 25, pPanel);
	pRunButton->SetMargin(2, 2);
	pRunButton->SetContainerFlags(CF_FIX_RIGHT | CF_FIX_TOP);
	pRunButton->SetText(L"Run");
	pRunButton->RegisterMessageHandler(WM_LBUTTONUP, &OnButtonClick);
#pragma endregion
	//=========

	// Comand buttons panel
#pragma region
	//		Panel
	pPanel = new UIPanel(NULL);
	pPanel->Initialize(0, 0, 100, 30, NULL, pMainWindow);
	StackLayout* pPanelStackLayout = new StackLayout(2, false, STACK_ORIENTATION_HORIZONTAL);
	pPanelStackLayout->SetMargin(2, 2, 2, 2);
	pPanel->SetLayoutProvider(pPanelStackLayout);

	//		Panel label
	UILabel* pLabel = new UILabel(NULL);
	pLabel->Initialize(0, 0, 100, 25, pPanel);
	pLabel->SetText(L"Control panel:");

	//		Command button for update process list
	UIButton* pUpdateButton = new UIButton(NULL);
	pUpdateButton->Initialize(0, 0, 100, 25, pPanel);
	pUpdateButton->SetText(L"Update");
	pUpdateButton->RegisterMessageHandler(WM_LBUTTONUP, WinMessageHandlerFunction_expr { UpdateProcessList(); return 0; });

	//		Command button for close selected process
	pCloseButton = new UIButton(NULL);
	pCloseButton->Initialize(0, 0, 100, 25, pPanel);
	pCloseButton->SetText(L"Close");
	pCloseButton->RegisterMessageHandler(WM_LBUTTONUP, &OnCloseButtonClick);

	//		Command button for change view state
	pChangePanels = new UIButton(NULL);
	pChangePanels->Initialize(0, 0, 150, 25, pPanel);
	pChangePanels->SetText(L"Show Devices");
	pChangePanels->RegisterMessageHandler(WM_LBUTTONUP, WinMessageHandlerFunction_expr 
		{
			if (pProcessPanel->IsVisible()) {
				pChangePanels->SetText(L"Show Process");
				pDevicePanel->SetVisible(true);
				pProcessPanel->SetVisible(false);
				UpdateDeviceList();
			} else {
				pChangePanels->SetText(L"Show Devices");
				pProcessPanel->SetVisible(true);
				pDevicePanel->SetVisible(false);
				UpdateProcessList();
			}
			return 0;
		});
#pragma endregion
	//===========

	// Down panel
	pProcessPanel = new UIPanel(NULL);
	pProcessPanel->Initialize(0, 0, 100, 100, NULL, pMainWindow);
	pStackLayout = new StackLayout(2, true, STACK_ORIENTATION_HORIZONTAL);
	pStackLayout->SetMargin(2, 2, 2, 2);
	pProcessPanel->SetLayoutProvider(pStackLayout);

	// Process Info Panel
#pragma region
	//		Panel
	pProcessPanelInfo = new UIPanel(NULL);
	pProcessPanelInfo->Initialize(0, 0, 300, 0, NULL, pProcessPanel);
	//		Stack layout for panel
	pStackLayout = new StackLayout(2, true);
	pProcessPanelInfo->SetLayoutProvider(pStackLayout);
	pProcessPanelInfo->SetVisible(false);
	//		Process name label
	pPPInfo_Name = new UILabel(NULL);
	pPPInfo_Name->Initialize(0, 0, 0, 20, pProcessPanelInfo);
	//		Process thread count label
	pPPInfo_ThreadCount = new UILabel(NULL);
	pPPInfo_ThreadCount->Initialize(0, 0, 0, 20, pProcessPanelInfo);
	//		Process thread list
	pPPInfo_Threads = new UIListView(NULL);
	pPPInfo_Threads->Initialize(10, 10, 100, 10, pProcessPanelInfo);
	pPPInfo_Threads->InsertColumn(UIListViewColumnDef(L"ID", 50, L"ID"));
	pPPInfo_Threads->InsertColumn(UIListViewColumnDef(L"Priority", 100, L"Priority"));
	pPPInfo_Threads->BindData((vector<IDataProvider*>*)&threadList);
	pPPInfo_Threads->Update();
#pragma endregion
	// ===

	pListView = new UIListView(NULL);
	pListView->Initialize(10, 80, 100, 100, pProcessPanel);
	pListView->SetMargin(10, 10);
	pListView->SetContainerFlags(CF_FIX_ALL);
	pListView->InsertColumn(UIListViewColumnDef(L"ID", 50, L"ID"));
	pListView->InsertColumn(UIListViewColumnDef(L"Process", 350, L"ProcessName"));
	pListView->InsertColumn(UIListViewColumnDef(L"Mem Usage", 100, L"MemoryUsage"));
	pListView->InsertColumn(UIListViewColumnDef(L"Thread count", 100, L"ThreadCount"));
	pListView->GetParent()->RegisterMessageHandler(WM_NOTIFY, &ListViewNotify);
	pListView->RegisterMessageHandler(WM_LBUTTONDBLCLK, &OnDblClick);
	pListView->RegisterMessageHandler(WM_KEYDOWN, &OnCloseButtonClick);
	pListView->RegisterMessageHandler(WM_KEYDOWN, &OnDblClick);
	pListView->RegisterMessageHandler(WM_KEYUP, &ListViewOnSelect);
	
	pListView->BindData((vector<IDataProvider*>*)&processList);
	pListView->Update();

	//=======================================================================
	// Devices
	pDevicePanel = new UIPanel(NULL);
	pDevicePanel->Initialize(0, 0, 0, 0, NULL, pMainWindow);
	pStackLayout = new StackLayout(2, true, STACK_ORIENTATION_HORIZONTAL);
	pDevicePanel->SetLayoutProvider(pStackLayout);

	pDevicePanelInfo = new UIPanel(NULL);
	pDevicePanelInfo->Initialize(0, 0, 300, 0, NULL, pDevicePanel);
	pStackLayout = new StackLayout(2, false, STACK_ORIENTATION_VERTICAL);
	pDevicePanelInfo->SetLayoutProvider(pStackLayout);
	pDevicePanelInfo->SetVisible(false);

	pDeviceListView = new UIListView(NULL);
	pDeviceListView->Initialize(0, 0, 0, 0, pDevicePanel);
	pDeviceListView->InsertColumn(UIListViewColumnDef(L"Device Name", 300, L"Name"));
	pDeviceListView->BindData((vector<IDataProvider*>*)&deviceList);
	pDeviceListView->Update();

	pDevicePanel->SetVisible(false);

	//=========================================================

	pMainWindow->Update();

	UpdateProcessList();

	pApplication->SetMainForm(pMainWindow);
	pApplication->Run();

	return 0;
}
